<#
.SYNOPSIS
Script to delete old files from temporary directories.

.DESCRIPTION
This is mainly intended to be used for cleaning up user-managed temporary directories, such as ~\Downloads or C:\temp\, as a task in Windows' Task Scheduler.
The script will scan through the specified folders, removing any files that haven't been accessed longer than the specified amount of time and then deleting any empty folders that remain.

.PARAMETER Path
Path(s) to scan for old files.

.PARAMETER Age
Amount of time to have passed for a file to be deleted.
A file will be deleted when the Creation Time, Last Write Time, and Last Access Time are all older than the given age.

.PARAMETER WhatIf
Shows what would happen if the script runs.
This flag is passed through to the Remove-Item cmdlet, and the script performs no special handling for folders that would be deleted entirely as a result of this script. Therefore, output with this flag enabled can only be considered accurate for files, and not directories.

.EXAMPLE
C:\PS> .\Remove-OldFiles.ps1 -Path C:\Temp\ -Age (New-Timespan -Days 30)

Deletes all files in C:\Temp\ older than 30 days.

.EXAMPLE
C:\PS> .\Remove-OldFiles.ps1 -Path ~\Downloads\, C:\Temp\ -Age (New-Timespan -Days 7)

Deletes all files in ~\Downloads and C:\Temp\ older than 7 days.
#>


[CmdletBinding(SupportsShouldProcess)]
Param (
    [Parameter(Mandatory=$True, Position=1)]
        [string[]]$Path,
    [Parameter(Mandatory=$True, Position=2)]
        [timespan]$Age
)

Set-StrictMode -Version Latest


# Array to hold files and folders to delete
$FilesToDelete = @()



function GetFilesToDelete {
    param (
        [string]$Path,
        [datetime]$Cutoff
    )

    $DeleteList = @()

    $Directories = Get-ChildItem -Path $Path -Directory
    foreach ($Directory in $Directories) {
        if ((Get-ChildItem $Directory | Measure-Object).Count -le 0 -and (IsItemOld -Path $Directory.FullName -Cutoff $Cutoff)) {
            $DeleteList += $Directory.FullName
        } else {
            [array]$DeletableFiles = GetFilesToDelete -Path $Directory -Cutoff $Cutoff
            $FileCount = (Get-ChildItem $Directory | Measure-Object).Count
            $DeleteCount = ($DeletableFiles | Measure-Object).Count
            Write-Debug "$($Directory.FullName): $($DeleteCount)/$($FileCount)"

            # Is the entire folder going to be deleted?
            if ((IsItemOld -Path $Directory.FullName -Cutoff $Cutoff) -and (Compare-Object -ReferenceObject (Get-ChildItem $Directory.FullName | Select-Object FullName) -DifferenceObject $DeletableFiles))
            {
                # Only add the directory to the list, the files within will get deleted with it.
                $DeleteList += $Directory.FullName
            } else {
                $DeleteList += $DeletableFiles
            }
        }
    }

    Get-ChildItem -Path $Path -File | Where-Object {-not $_.PSIsContainer `
            -and (IsItemOld -Path $_.FullName -Cutoff $Cutoff)
        } | ForEach-Object {$DeleteList += $_.FullName}

    return $DeleteList
}

function IsItemOld {
    param (
        [string]$Path,
        [datetime]$Cutoff
    )
    $Item = Get-Item -LiteralPath $Path
    If ($Item.CreationTime -lt $Cutoff `
            -and $Item.LastWriteTime -lt $Cutoff `
            -and $Item.LastAccessTime -lt $Cutoff) {
        $true
    } else {
        $false
    }
}

$Cutoff = (Get-Date) - $Age

foreach ($Directory in $Path) {
    $FilesToDelete += GetFilesToDelete -Path $Directory -Cutoff $Cutoff
}

$FilesToDelete | Sort-Object | ForEach-Object {
    Remove-Item -LiteralPath $_ -Recurse
}
