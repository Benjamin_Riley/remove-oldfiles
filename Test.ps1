<#
TREE /F listing for generated folder:
    Folder PATH listing
    Volume serial number is 4035-B023
    C:.
    │   NewFile
    │   OldFile
    │   
    ├───NewDir
    │       NewFile
    │       
    ├───NewEmptyDir
    ├───OldDir
    │       OldFile
    │       
    ├───OldDirPartialDelete
    │       NewFile
    │       OldFile
    │       
    ├───OldEmptyDir
    └───RecursiveDir
        │   NewFile
        │   OldFile
        │   
        ├───NewDir
        │       NewFile
        │       
        ├───NewEmptyDir
        ├───OldDir
        │       OldFile
        │       
        ├───OldDirPartialDelete
        │       NewFile
        │       OldFile
        │       
        └───OldEmptyDir

TREE /F listing for expected final folder:
    Folder PATH listing
    Volume serial number is 4035-B023
    C:.
    │   NewFile
    │   
    ├───NewDir
    │       NewFile
    │       
    ├───NewEmptyDir
    ├───OldDirPartialDelete
    │       NewFile
    │       
    └───RecursiveDir
        │   NewFile
        │   
        ├───NewDir
        │       NewFile
        │       
        ├───NewEmptyDir
        └───OldDirPartialDelete
                NewFile
#>

# Folder to generate test files in
$TestFolder = Join-Path -Path $PSScriptRoot -ChildPath "Test"

$Cutoff = New-TimeSpan -Days 1
$OldDate = (Get-Date) - $Cutoff

# List of paths that should remain after the script has been run
$ExpectedPaths = @()

# List of paths that should have been deleted by the script
$UnexpectedPaths = @()


function CreatePath {
    param (
        [string]$Name,
        [switch]$IsDirectory = $false,
        [switch]$IsOld = $false,
        [switch]$ExpectDeletion = $false
    )

    $FullPath = Join-Path -Path (Get-Location) -ChildPath $Name
    New-Item $FullPath -ItemType ($IsDirectory ? "directory" : "file")

    if ($IsOld) {
        ForgeDates -Path $FullPath
    }

    if ($ExpectDeletion) {
        $script:UnexpectedPaths += $FullPath
    } else {
        $script:ExpectedPaths += $FullPath
    }
}

function ForgeDates {
    param (
        $Path
    )
    $(Get-Item $Path).CreationTime = $OldDate
    $(Get-Item $Path).LastWriteTime = $OldDate
    $(Get-Item $Path).LastAccessTime = $OldDate
}

# Remove old test folder, if it exists.
if (Test-Path $TestFolder) {
    Write-Warning "Test directory ($($TestFolder)) already exists, removing."
    Remove-Item $TestFolder -Recurse
}


# Create test folder
Write-Host "Creating folder structure"

New-Item $TestFolder -ItemType "directory"
Push-Location $TestFolder
    # Create files
    CreatePath -Name "NewFile"
    CreatePath -Name "OldFile" -IsOld -ExpectDeletion

    CreatePath -Name "NewDir" -IsDirectory
    Set-Location "NewDir"
        CreatePath -Name "NewFile"
    Set-Location ..

    CreatePath -Name "NewEmptyDir" -IsDirectory

    CreatePath -Name "OldDir" -IsDirectory -IsOld -ExpectDeletion
    Set-Location "OldDir"
        CreatePath -Name "OldFile" -IsOld -ExpectDeletion
        ForgeDates -Path .
    Set-Location ..

    CreatePath -Name "OldDirPartialDelete" -IsDirectory -IsOld
    Set-Location "OldDirPartialDelete"
        CreatePath -Name "NewFile"
        CreatePath -Name "OldFile" -IsOld -ExpectDeletion
    Set-Location ..

    CreatePath -Name "OldEmptyDir" -IsOld -IsDirectory -ExpectDeletion

    CreatePath -Name "RecursiveDir" -IsDirectory
        Set-Location "RecursiveDir"
        CreatePath -Name "NewFile"
        CreatePath -Name "OldFile" -IsOld -ExpectDeletion

        CreatePath -Name "NewDir" -IsDirectory
        Set-Location "NewDir"
            CreatePath -Name "NewFile"
        Set-Location ..

        CreatePath -Name "NewEmptyDir" -IsDirectory

        CreatePath -Name "OldDir" -IsDirectory -IsOld -ExpectDeletion
        Set-Location "OldDir"
            CreatePath -Name "OldFile" -IsOld -ExpectDeletion
            ForgeDates -Path .
        Set-Location ..

        CreatePath -Name "OldDirPartialDelete" -IsDirectory -IsOld
        Set-Location "OldDirPartialDelete"
            CreatePath -Name "NewFile"
            CreatePath -Name "OldFile" -IsOld -ExpectDeletion
        Set-Location ..

        CreatePath -Name "OldEmptyDir" -IsOld -IsDirectory -ExpectDeletion
    Set-Location ..
Pop-Location


# Run script
Push-Location $PSScriptRoot
    .\Remove-OldFiles.ps1 -Path $TestFolder -Age $Cutoff
Pop-Location


# Test remaining files
$Success = $true

$ExpectedPaths | Sort-Object | ForEach-Object {
    if (-Not (Test-Path $_)) {
        $Success = $false
        Write-Error "Missing expected item $($_)"
    }
}
$UnexpectedPaths | Sort-Object | ForEach-Object {
    if (Test-Path $_) {
        $Success = $false
        Write-Error "Failed to delete $($_)"
    }
}

if ($Success) {
    Write-Host "Test passed"
} else {
    Write-Host "Test failed"
}
